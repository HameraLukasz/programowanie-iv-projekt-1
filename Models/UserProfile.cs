﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table("userprofiles")]
    public class UserProfile
    {
        [Key]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BornDate { get; set; }
        public Education Education { get; set; }
        public virtual ICollection<Hobby> Hobby { get; set; }
        public Lifestyle Lifestyle { get; set; }
        public virtual ICollection<CulinaryPreferences> CulinaryPreferences { get; set; }
        public Religion Religion { get; set; }
        public string Description { get; set; }
        public Children Children { get; set; }
    }
}