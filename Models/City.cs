﻿using GoogleMaps.LocationServices;

namespace Models
{
    internal class City
    {
        public City(string city)
        {
            Name = city;

            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(city);

            Latitude = point.Latitude;
            Longitude = point.Longitude;
        }

        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}