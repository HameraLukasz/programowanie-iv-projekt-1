﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Mail;
using System.Data.Entity;

namespace Models
{
    [Table("useraccounts")]
    public class UserAccount
    {
        [Key]
        public int Id { get; set; }
        public UserProfile UserProfile { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public bool? Status { get; set; }
    }
}