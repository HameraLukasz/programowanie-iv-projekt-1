﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Context
{
    public class MyContextFactory : System.Data.Entity.Infrastructure.IDbContextFactory<UserAccountContext>
    {
        public UserAccountContext Create()
        {
            return new UserAccountContext($"Data Source = localhost; Initial Catalog = ProjektDatabase; Integrated Security = SSPI; ");
        }
    }
}
