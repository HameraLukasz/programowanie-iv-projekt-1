﻿using System.Data.Entity;

namespace Models
{
    public class UserAccountContext : DbContext
    {
        public UserAccountContext(string connectionString)
            : base(connectionString)
        {
        }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<UserAccount> Users { get; set; }
    }
}