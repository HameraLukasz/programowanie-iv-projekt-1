﻿namespace Models
{
    internal class Message
    {
        public int IdMessage { get; set; }
        public UserAccount Creator { get; set; }
        public UserAccount Recipient { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; }
    }
}