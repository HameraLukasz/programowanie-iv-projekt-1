namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.userprofiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        BornDate = c.DateTime(nullable: false),
                        Education = c.Int(nullable: false),
                        Lifestyle = c.Int(nullable: false),
                        Religion = c.Int(nullable: false),
                        Description = c.String(),
                        Children = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.useraccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Password = c.Binary(),
                        Status = c.Boolean(),
                        UserProfile_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.userprofiles", t => t.UserProfile_ID)
                .Index(t => t.UserProfile_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.useraccounts", "UserProfile_ID", "dbo.userprofiles");
            DropIndex("dbo.useraccounts", new[] { "UserProfile_ID" });
            DropTable("dbo.useraccounts");
            DropTable("dbo.userprofiles");
        }
    }
}
