using System.Collections.Generic;

namespace Models.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Models.UserAccountContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Models.UserAccountContext context)
        {

                context.Users.AddOrUpdate(
                    new UserAccount()
                    {
                        Id = 1,
                        Email = "hameran@wp.pl",
                        Password = new byte[2],
                        UserProfile = new UserProfile()
                        {
                            ID = 1,
                            BornDate = new DateTime(2001, 1, 1),
                            Children = Children.None,
                            CulinaryPreferences = new List<CulinaryPreferences>() { CulinaryPreferences.None },
                            Description = " ",
                            Education = Education.Podstawowe,
                            FirstName = "Jan",
                            Hobby = new List<Hobby>() { Hobby.None },
                            LastName = "Kowalski",
                            Lifestyle = Lifestyle.None,
                        }
                    }
                );

        }
    }
}
