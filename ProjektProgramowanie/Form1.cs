﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Models;

namespace ProjektProgramowanie
{
    public partial class MainForm : MetroForm
    {
        public MainForm()
        {
            InitializeComponent();
        }
        private static List<Education> _educations = new List<Education>()
        {
            Education.Podstawowe,
            Education.Zawodowe
        };

        private void metroButton1_Click(object sender, EventArgs e)
        {
            foreach (var item in _educations)
            {
                metroComboBox1.Items.Add(item);

            }

            using (var context = new UserAccountContext($"Data Source=localhost;Initial Catalog=ProjektDatabase;Integrated Security=SSPI;"))
            {
                context.Users.Add(new UserAccount()
                {
                    Id = 1,
                    Email = "hameran@wp.pl",
                    Password = new byte[2],
                    UserProfile = new UserProfile()
                    {
                        ID = 1,
                        BornDate = new DateTime(2001,1,1),
                        Children = Children.None,
                        CulinaryPreferences = new List<CulinaryPreferences>() { CulinaryPreferences.None},
                        Description = " ",
                        Education = Education.Podstawowe,
                        FirstName = "Jan",
                        Hobby = new List<Hobby>() { Hobby.None},
                        LastName = "Kowalski",
                        Lifestyle = Lifestyle.None,
                    }
                });

                context.SaveChanges();
            }
        }

        private void metroCheckBox2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
